module.exports = function(grunt) {

  // Project configuration.
 grunt.initConfig({
  sass: {                              // Task
    dist: {                            // Target
      options: {                       // Target options
        style: 'expanded'
      },
      files: {                         // Dictionary of files
        'style/style.css': 'source/main.scss',       // 'destination': 'source'
      }
    }
  }
});
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);
  grunt.registerTask('default', ['sass']);

};